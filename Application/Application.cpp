#include "stdafx.h"
#include "..\CPU\CPU.h"
#include "..\CPU\my_vector.h"

void printFile(const char* fName) {
	ifstream pic(fName);
	string line;
	while (std::getline(pic, line)){
		std::cout << line << std::endl;
	}
}

int runFile(CPU& testSample) {
	cout << "Enter file name:" << endl;
	string fName;
	cin >> fName;
	if (!cin.good())
		return 0;
	ifstream in(fName);
	if (in.is_open()) {
		cout << "RUNNING..." << endl;
		try{
			testSample.executeProgram(in);
		}
		catch (exception& bm) {
			cout << "FAIL:" << endl;
			cout << bm.what() << endl;
		}
		cout << endl;
		cout << "SUCCESS" << endl;

		in.close();
		return 1;
	}
	
	cout << "Cannot open file" << endl;
	return 1;
}

int printRegisters(CPU& testSample) {
	testSample.printRegisters();
	return 1;
}

int printMemory(CPU& testSample) {
	testSample.printDataMem();
	return 1;
}

int enterProgram(CPU&) {
	CPU s;
	cout << "Start to write program:" << endl;
	try{
		s.executeProgram(cin);
	}
	catch (exception& bm){ cout << "FAIL:\n" << bm.what() << endl; return 1; }
	cout << "SUCCESS" << endl;
	const char *msgs[] = { "0. Quit to the main menu", "1. Print registers", "2. Print memory"};
	const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
	int rc;
	int(*fptr[])(CPU&) = { nullptr, printRegisters, printMemory };
	while (rc = dialog(msgs, NMsgs))
		if (!fptr[rc](s))
			break;
	return 1;
}


int enterCmds(CPU&s) {
	s.flushMemory();
	Program prog;
	try{
		prog.addCmd();
		prog.runProgram(s);
	}
	catch (exception& bm){ cout << bm.what() << endl; };
	return 1;
}

int printVariables(CPU& s) {
	s.printDataMem();
	return 1;
}

int memFlush(CPU& testSample) {
	testSample.flushMemory();
	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	const char *msgs[] = { "0. Quit", "1.Run file", "2.Enter program", "3.Print registers", "4.Print variables", "5.Enter commands", "6.Flush memory" };
	const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
	int rc;
	CPU s;
	int(*fptr[])(CPU&) = { nullptr, runFile, enterProgram, printRegisters, printVariables, enterCmds ,memFlush  };
	while (rc = dialog(msgs, NMsgs))
		if (!fptr[rc](s))
			break;
	cout << "That's all. Bye!" << endl;
	return 0;
}




