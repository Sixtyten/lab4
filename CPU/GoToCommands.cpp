#pragma once
#include "GoToCommands.h"
#include "CPU.h"

ostream& operator<<(ostream& os, const GoToRegisterLabel& ex){
	ex.print(os);
	os << ex.target << endl;
	return os;
}

istream& operator>>(istream& is, GoToRegisterLabel& ex) {
	ex.input(is);
	cout << "Enter target: " << endl;
	is >> ex.target;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void GoToRegisterLabel::execute() {
	int res = -1;
	if (fptr != nullptr) {
		int* EF = &regBlock->flags.find("EF")->second.first;
		int* ZF = &regBlock->flags.find("ZF")->second.first;
		res = fptr(EF, ZF);
	}
	if (fptr == nullptr || res == 1) {
		int targetValue;
		targetValue = regBlock->getValue(target);
		cmdMemory->setIP(targetValue);
	}
}

void GoToRegisterLabel::initCmd(const CPU& proc){
		auto val = instrTable.find(getCmdCode());
		if (val == instrTable.end())
			throw(exception("command not found"));
		fptr = val->second;
		cmdMemory = const_cast<CpuMem*>(&proc.cmdMemory);
		regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
}

ostream& operator<<(ostream& os, const GoToLabel& ex){
	ex.print(os);
	os << ex.target << endl;
	return os;
}

istream& operator>>(istream& is, GoToLabel& ex) {
	ex.input(is);
	cout << "Enter target: " << endl;
	is >> ex.target;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void GoToLabel::execute() {
	int res = -1;
	if (fptr != nullptr) {
		int* EF = &regBlock->flags.find("EF")->second.first;
		int* ZF = &regBlock->flags.find("ZF")->second.first;
		res = fptr(EF, ZF);
	}
	if (fptr == nullptr || res == 1) {
		cmdMemory->executeGoToCmd(target);
	}
}

void GoToLabel::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	cmdMemory = const_cast<CpuMem*>(&proc.cmdMemory);
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
}

void GoToAdress::execute(){
	int res = -1;
	if (fptr != nullptr) {
		int* EF = &regBlock->flags.find("EF")->second.first;
		int* ZF = &regBlock->flags.find("ZF")->second.first;
		res = fptr(EF, ZF);
	}
	if (fptr == nullptr || res == 1) {
		if (target < 0 || target > cmdMemory->getInstrNum())
			throw(exception("invalid adress"));
		cmdMemory->setIP(target);
	}
}

void GoToAdress::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	cmdMemory = const_cast<CpuMem*>(&proc.cmdMemory);
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
}

ostream& operator<<(ostream& os, const GoToAdress& ex){
	ex.print(os);
	os << ex.target << endl;
	return os;
}

istream& operator>>(istream& is, GoToAdress& ex) {
	ex.input(is);
	cout << "Enter target: " << endl;
	is >> ex.target;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}
