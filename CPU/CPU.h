#pragma once

#include "AbstractCommand.h"
#include "ControlUnit.h"
#include "CpuMem.h"
#include "DataMem.h"
#include "ExecuteUnit.h"
#include "RegistersBlock.h"
#include "GoToCommands.h"
#include "Program.h"
#include "my_vector.h"

using namespace std;

enum cmdTableFields
{
	FIRST_OPERAND,
	SECOND_OPERAND,
	CMD_OPCODE,
	CMD_LEN,
	CMD_TYPE
};

const int END_STATEMENT = -2;
const int IS_DIRECTIVE = -3;
const int IS_LABEL = -4;

class CPU
{
private:
	CpuMem memory;
	RegistersBlock regBlock;
	CpuMem cmdMemory;
	DataMem dataMemory;
	ExecuteUnit exUnit;
	ControlUnit ctrlUnit;

	multimap<string, int> cmdTable;
	map<string, pair<int, int>> labelTable;
	map<string, int> directiveTable;

	void cmdTableInit(const string&);
	bool checkType(const string&, int type)const;
	void directiveTableInit(const string&);
	int extractType(const vctr::vector<string>&);
	vctr::vector<string> split(const string &, char);
	int getDirLength(const string&);


	void passOne(istream&);
	void passTwo();

	void addRegRegCmd(const string&, const string&, const string&);
	void addRegMemCmd(const string&, const string&, const string&);
	void addRegConstCmd(const string&, const string&, int);
	void addRegCmd(const string&, const string&);
	void addMemMemCmd(const string&, const string&, const string&);
	void addMemRegCmd(const string&, const string&, const string&);
	void addMemConstCmd(const string&, const string&, int);
	void addMemCmd(const string&, const string&);
	void addGoToReg(const string&, const string&);
	void addGoToMem(const string&, const string&);
	void addGoToAdress(const string&, int);
	void addVar(const string&, const string&, int);
public:
	void executeProgram(istream&);
	CPU() :cmdMemory(&regBlock), ctrlUnit(&regBlock, &dataMemory, &cmdMemory, &exUnit), regBlock("reg.txt", "flags.txt"){};
	void printRegisters(){ regBlock.print(); };
	void printDataMem(){ dataMemory.printAll(); };
	void loadAndRun(Program&);
	void flushMemory();
	void loadData(int, const string&, const string&);
	~CPU(){};

















	friend class RegRegCommand;
	friend class RegMemCommand;
	friend class RegConstCommand;
	friend class RegCommand;
	friend class MemMemCommand;
	friend class MemRegCommand;
	friend class MemConstCommand;
	friend class MemCommand;
	friend class GoToRegisterLabel;
	friend class GoToLabel;
	friend class GoToAdress;
};