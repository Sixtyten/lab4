#include "DataMem.h"

void DataMem::allocateMemory(const string& label, int size, int value) {
	char* ptr = new char[size+1];
	sprintf_s(ptr, size+1, "%d", value);
	try{
		getData(label);
	} catch (exception&){
		data.insert({ label, tuple<char*, int, bool>{ ptr, size, false } }); 
		return;
	}
	throw(exception("label is already exists"));
}

int DataMem::getData(const string& label)const {
	auto value = data.find(label);
	if (value != data.end()) 
		return stoi(get<0>(value->second));
	throw(std::exception("label is not exists"));
}

int DataMem::getSize(const string& label)const {
	auto pos = data.find(label);
	if (pos == data.end())
		throw(exception("object not found"));
	return get<1>(pos->second);
}

bool DataMem::isLocked(const string& label)const {
	auto pos = data.find(label);
	if (pos == data.end())
		throw(exception("object not found"));
	return get<2>(pos->second);
}

void DataMem::changeData(const string& label, int size, int value) {
	auto pos = data.find(label);
	if (pos == data.end())
		throw(exception("label is not exist"));
	char* ptr = get<0>(pos->second);
	data.erase(pos);
	allocateMemory(label, size, value);
	delete[] ptr;
}

void DataMem::changeLock(const string& label, bool boolean) {
	auto value = data.find(label);
	if (value != data.end()) {
		value->second = tuple<char*, int, bool>(get<0>(value->second), get<1>(value->second), boolean);
		return;
	}
	throw(exception("label is not exists"));
}

void DataMem::flushData() {
	for (auto pos = data.begin(); pos != data.end(); pos++) {
		char* ptr = get<0>(pos->second);
		pos->second = tuple<char*, int, bool>(nullptr, get<1>(pos->second), get<2>(pos->second));
		delete[] ptr;
	}
	data.clear();
}

tuple<char*, int, bool> DataMem::getTuple(const string& label)const{
	auto pos = data.find(label);
	if (pos == data.end())
		throw(exception("label not found"));
	return pos->second;
}