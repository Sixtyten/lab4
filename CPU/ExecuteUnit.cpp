#include "ExecuteUnit.h"

void ExecuteUnit::ExecuteCommand(AbstractCommand* ptr) {
	isLocked = true;
	ptr->execute();
	isLocked = false;
}