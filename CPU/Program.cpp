//#include "Program.h"
#include "CPU.h"

int getInt(int &a) {
	cin >> a;
	if (cin.eof())
		return -2;
	if (!cin.good()){
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		return -1;
	}
	return 1;
}

int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;
	do{
		if (errmsg != "")
			puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";
		for (i = 0; i < N; ++i)
			puts(msgs[i]);
		puts("Make your choice: --> ");
		n = getInt(rc);
		if (n == 0)
			rc = 0;
		if (n == -2)
			exit(0);
	} while (rc < 0 || rc >= N);
	return rc;
}


void Program::initCommands(const CPU& proc) {
	for (auto pos = commands.begin(); pos != commands.end(); pos++)
		(*pos)->initCmd(proc);
}


AbstractCommand* addRegReg() {
	RegRegCommand* ptr = new RegRegCommand;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addRegMem() {
	RegMemCommand* ptr = new RegMemCommand;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addRegConst() {
	RegConstCommand* ptr = new RegConstCommand;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addReg() {
	RegCommand* ptr = new RegCommand;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addMemMem() {
	MemMemCommand* ptr = new MemMemCommand;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addMemReg() {
	MemRegCommand* ptr = new MemRegCommand;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addMemConst() {
	MemConstCommand* ptr = new MemConstCommand;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addMem() {
	MemCommand* ptr = new MemCommand;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addGoToRegLabel() {
	GoToRegisterLabel* ptr = new GoToRegisterLabel;
	cin >> *ptr;
	return ptr;
}

AbstractCommand* addGoToLabel() { 
	GoToLabel* ptr = new GoToLabel;
	cin >> *ptr;
	return ptr;
}

pair<int, pair<string, string>> addVar() {
	const char* errmsg ="";
	int n, rc;
	cout << "Enter value:" << endl;
	do{
		if (errmsg != "")
			puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";
		n = getInt(rc);
		if (n == 0)
			rc = 0;
		if (n == -2)
			exit(0);
	} while (rc < 0 );
	string label, directive;
	cout << "Enter directive:" << endl;
	cin >> directive;
	cout << "Enter label:" << endl;
	cin >> label;
	if (!cin)
		return pair<int, pair<string, string>>(0, pair<string, string>("", ""));
	return pair<int, pair<string, string>>(rc, pair<string, string>(directive, label));
}

AbstractCommand* addGoToAdress() { 
	GoToAdress* ptr = new GoToAdress;
	cin >> *ptr;
	return ptr;
}

void Program::addCmd() {
	const char *msgs[] = { "0. Quit", "1.Add R/R command", "2.Add R/M command", "3.Add R command", "4.Add M/M command",
		"5.Add M/R command", "6.Add M command", "7.Add R/C command", "8.Add M/C",
		"9.Add go to Register value", "10.Add go to label", "11.Add go to adress" ,"12.Add variable" };
	const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
	int rc;
	AbstractCommand* newCmd;
	AbstractCommand*(*fptr[])() = { nullptr, addRegReg, addRegMem, addReg, addMemMem, addMemReg, addMem, addRegConst, addMemConst, 
		addGoToRegLabel, addGoToLabel, addGoToAdress };
	while (rc = dialog(msgs, NMsgs)) {
		if (rc == 12) {
			auto value = addVar();
			data.push_back(value);
			continue;
		}
		newCmd = fptr[rc]();
		if (!newCmd)
			return;
		commands.push_back(newCmd);
	}
}

void Program::runProgram(CPU& proc) {
	initCommands(proc);
	for (auto pos = data.begin(); pos != data.end(); pos++)
		proc.loadData(pos->first, pos->second.first, pos->second.second);
	proc.loadAndRun(*this);
}