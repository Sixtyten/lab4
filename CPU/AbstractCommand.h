#pragma once

#include <iostream> 
#include <tuple>
#include <string>
#include <map>

#include "RegistersBlock.h"

#include "DataMem.h"

using namespace std;

class CPU;

class AbstractCommand
{
public:
	AbstractCommand(): label(""){};
	AbstractCommand(const string& labelName, const string& cmdName) : label(labelName), cmdCode(cmdName) {}
	AbstractCommand(const char* str) : cmdCode(str){};
	AbstractCommand(const string& str) : cmdCode(str){};
	virtual ~AbstractCommand(){};
	void setCmdCode(const string& opCode){ cmdCode = opCode; }
	string getCmdCode()const { return cmdCode; }
	void setLabel(const string& str){ label = str; };
	string getLabel(){ return label; }
	virtual void initCmd(const CPU& proc) = 0;
	virtual void execute() = 0;
	virtual AbstractCommand* clone() = 0;
protected:
	void print(ostream& os) const;
	void input(istream& is);
private:
	string label;
	string cmdCode;
};

class RegRegCommand :
	public AbstractCommand 
{
public:
	RegRegCommand() :AbstractCommand(){};
	RegRegCommand(const string& cmdName, const string& label, const string& strName1, const string& strName2) :
		AbstractCommand(label, cmdName), name1(strName1), name2(strName2) {};
	RegRegCommand(const string& cmdName, const string& strName1, const string& strName2, RegistersBlock* regs, int(*funcPtr)(int*, int*))
		:AbstractCommand(cmdName), name1(strName1), name2(strName2), regBlock(regs), op1(nullptr), op2(nullptr), fptr(funcPtr) {}
	~RegRegCommand(){};
	void execute();
	void initCmd(const CPU& proc);
	AbstractCommand* clone(){ RegRegCommand* ptr = new RegRegCommand(*this); return ptr; };
	friend ostream& operator<<(ostream&, const RegRegCommand&);
	friend istream& operator>>(istream&, RegRegCommand&);
private:
	string name1;
	string name2;
	RegistersBlock* regBlock;
	pair<int, bool>* op1;
	pair<int, bool>* op2;
	int(*fptr)(int*, int*);
};


class RegMemCommand :
	public AbstractCommand
{
public:
	RegMemCommand(const string& cmdName, const string& label, const string& strName1, const string& strName2) :
		AbstractCommand(label, cmdName), name1(strName1), name2(strName2){};
	RegMemCommand(const string& cmdName, const string& strName1, const string& strName2, RegistersBlock* regs, DataMem* data ,int(*funcPtr)(int*, int*))
		:AbstractCommand(cmdName), name1(strName1), name2(strName2), regBlock(regs), dataBlock(data), op1(nullptr), fptr(funcPtr) {}
	RegMemCommand() :AbstractCommand(){}
	~RegMemCommand(){};
	void execute();
	void initCmd(const CPU& proc);
	AbstractCommand* clone(){ RegMemCommand* ptr = new RegMemCommand(*this); return ptr; };
	friend ostream& operator<<(ostream&, const RegMemCommand&);
	friend istream& operator>>(istream&, RegMemCommand&);
private:
	string name1;
	string name2;
	RegistersBlock* regBlock;
	DataMem* dataBlock;
	pair<int, bool>* op1;
	int(*fptr)(int*, int*);
};

class RegConstCommand :
	public AbstractCommand
{
public:
	RegConstCommand():AbstractCommand(){};
	RegConstCommand(const string& cmdName, const string& label, const string& strName1, int constValue)
		:AbstractCommand(label, cmdName), name1(strName1), op2(constValue){};
	RegConstCommand(const string& cmdName, const string& strName1, int constValue, RegistersBlock* regs, int(*funcPtr)(int*, int*))
		:AbstractCommand(cmdName), name1(strName1), regBlock(regs), op1(nullptr), op2(constValue), fptr(funcPtr) {};
	~RegConstCommand(){};
	void execute();
	void initCmd(const CPU& proc);
	AbstractCommand* clone(){ RegConstCommand* ptr = new RegConstCommand(*this); return ptr; };
	friend ostream& operator<<(ostream&, const RegConstCommand&);
	friend istream& operator>>(istream&, RegConstCommand&);
private:
	string name1;
	RegistersBlock* regBlock;
	pair<int, bool>* op1;
	int op2;
	int(*fptr)(int*, int*);
};

class RegCommand :
	public AbstractCommand
{
public:
	RegCommand() :AbstractCommand(){};
	RegCommand(const string& cmdName, const string& label, const string& strName1)
		:AbstractCommand(label, cmdName), name1(strName1){};
	RegCommand(const string& cmdName, const string& strName1, RegistersBlock* regs, int(*funcPtr)(int*, int*))
		:AbstractCommand(cmdName), name1(strName1), regBlock(regs), fptr(funcPtr) {}
	~RegCommand(){};
	void execute();
	void initCmd(const CPU& proc);
	AbstractCommand* clone(){RegCommand* ptr = new RegCommand(*this); return ptr;};
	friend ostream& operator<<(ostream&, const RegCommand&);
	friend istream& operator>>(istream&, RegCommand&);
private:
	string name1;
	RegistersBlock* regBlock;
	pair<int, bool>* op;
	int(*fptr)(int*, int*);
};

class MemMemCommand :
	public AbstractCommand
{
public:
	MemMemCommand():AbstractCommand(){};
	MemMemCommand(const string& label, const string& cmdName, const string& strName1, const string& strName2)
		:AbstractCommand(label, cmdName), name1(strName1), name2(strName2){};
	MemMemCommand(const string& cmdName, const string& strName1, const string& strName2, DataMem* data, int(*funcPtr)(int*, int*), RegistersBlock* regs)
		:AbstractCommand(cmdName), name1(strName1), name2(strName2), dataBlock(data), fptr(funcPtr), regBlock(regs) {}
	~MemMemCommand(){};
	void execute();
	void initCmd(const CPU& proc);
	AbstractCommand* clone(){ MemMemCommand* ptr = new MemMemCommand(*this); return ptr; };
	friend ostream& operator<<(ostream&, const MemMemCommand&);
	friend istream& operator>>(istream&, MemMemCommand&);
private:
	string name1;
	string name2;
	RegistersBlock* regBlock;
	DataMem* dataBlock;
	int(*fptr)(int*, int*);
};

class MemRegCommand :
	public AbstractCommand
{
public:
	MemRegCommand():AbstractCommand(){};
	MemRegCommand(const string& label, const string& cmdName, const string& strName1, const string& strName2)
		:AbstractCommand(label, cmdName), name1(strName1), name2(strName2){};
	MemRegCommand(const string& cmdName, const string& strName1, const string& strName2, RegistersBlock* regs, DataMem* data, int(*funcPtr)(int*, int*))
		:AbstractCommand(cmdName), name1(strName1), name2(strName2), regBlock(regs), dataBlock(data), op2(nullptr), fptr(funcPtr) {};
	~MemRegCommand(){};
	void execute();
	void initCmd(const CPU& proc);
	AbstractCommand* clone(){ MemRegCommand* ptr = new MemRegCommand(*this); return ptr; };
	friend ostream& operator<<(ostream&, const MemRegCommand&);
	friend istream& operator>>(istream&, MemRegCommand&);
private:
	string name1;
	string name2;
	std::pair<int, bool>* op2;
	DataMem* dataBlock;
	RegistersBlock* regBlock;
	int(*fptr)(int*, int*);
};

class MemConstCommand :
	public AbstractCommand
{
public:
	MemConstCommand():AbstractCommand(){};
	MemConstCommand(const string& label, const string& cmdName, const string& strName1, int constVal)
		:AbstractCommand(label, cmdName), name1(strName1), op2(constVal){};
	MemConstCommand(const string& cmdName, const string& strName1, DataMem* data, int constValue, int(*funcPtr)(int*, int*), RegistersBlock* regs)
		:AbstractCommand(cmdName), name1(strName1), dataBlock(data), op2(constValue), fptr(funcPtr), regBlock(regs) {}
	~MemConstCommand(){};
	void execute();
	void initCmd(const CPU& proc);
	AbstractCommand* clone(){MemConstCommand* ptr = new MemConstCommand(*this); return ptr;};
	friend ostream& operator<<(ostream&, const MemConstCommand&);
	friend istream& operator>>(istream&, MemConstCommand&);
private:
	string name1;
	RegistersBlock* regBlock;
	DataMem* dataBlock;
	int op2;
	int(*fptr)(int*, int*);
};

class MemCommand :
	public AbstractCommand
{
public:
	MemCommand(const string& label, const string& cmdName, const string& strName1)
		:AbstractCommand(label, cmdName), name1(strName1){};
	MemCommand():AbstractCommand(){};
	MemCommand(const string& cmdName, const string& strName1, DataMem* data, int(*funcPtr)(int*, int*))
		:AbstractCommand(cmdName), name1(strName1), dataBlock(data), fptr(funcPtr) {}
	~MemCommand(){};
	void execute();
	void initCmd(const CPU& proc);
	AbstractCommand* clone(){ MemCommand* ptr = new MemCommand(*this); return ptr; };
	friend ostream& operator<<(ostream&, const MemCommand&);
	friend istream& operator>>(istream&, MemCommand&);
private:
	string name1;
	DataMem* dataBlock;
	int(*fptr)(int*, int*);
};


