#pragma once

#include "AbstractCommand.h"
#include "DataMem.h"
#include "ExecuteUnit.h"
#include "RegistersBlock.h"

class ControlUnit
{
public:
	ControlUnit(RegistersBlock* rBlock, DataMem* dMem, CpuMem* cMem, ExecuteUnit* eUnit)
		:regBlock(rBlock), dataMem(dMem), cmdMem(cMem), exUnit(eUnit){};
	ControlUnit(){};
	~ControlUnit(){};
	void setCmd();
	void ExecuteCmd(AbstractCommand*);
private:
	RegistersBlock* regBlock;
	DataMem* dataMem;
	CpuMem* cmdMem;
	ExecuteUnit* exUnit;
	int flagsRegister;
};
