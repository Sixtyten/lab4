#pragma once

#include <stdio.h>
#include <map>
#include <tuple>
#include <string>
#include <iostream>
using namespace std;

class DataMem
{
public:
	DataMem(){};
	~DataMem(){ for (auto elem : data) { char* ptr = get<0>(elem.second); delete[] ptr; } };
	int getData(const string&)const;
	int getSize(const string&)const;
	bool isLocked(const string&)const;
	tuple<char*, int, bool> getTuple(const string&)const;
	void changeData(const string&, int, int);
	void allocateMemory(const string& , int, int);
	void changeLock(const string&, bool);
	void printAll(){ for (auto pos = data.cbegin(); pos != data.cend(); pos++) cout << pos->first << ":\t" << getData(pos->first) << endl; }
	void flushData();
private:
	map<string, tuple<char*, int, bool> > data;
	friend class MemCommand;
	friend class MemRegCommand;
	friend class MemMemCommand;
	friend class MemConstCommand;
};
