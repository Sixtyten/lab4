#include "AbstractCommand.h"
#include "CPU.h"

void AbstractCommand::input(istream& is) {
	cout << "Enter opCode:" << endl;
	is >> cmdCode;
	if (!is.good())
		throw(exception("EOF occured"));
	string res;
	cout << "Do you want to enter label? (Y/N)" << endl;
	is >> res;
	if (!is.good())
		throw(exception("EOF occured"));
	if (res == "Y" || res == "y" || res == "�" || res == "�") {
		cout << "Enter label:" << endl;
		is >> label;
		if (!is.good())
			throw(exception("EOF occured"));
	}
}

void AbstractCommand::print(ostream& os)const {
	os << label << endl;
	os << cmdCode << endl;
}

ostream& operator<<(ostream&os, const RegRegCommand&ex){
	ex.print(os);
	os << ex.name1 << endl;
	os << ex.name2 << endl;
	return os;
}

istream& operator>>(istream& is, RegRegCommand& ex) {
	ex.input(is);
	cout << "Enter register1: " << endl;
	is >> ex.name1;
	if (!is.good())
		throw(exception("EOF occured"));
	cout << "Enter register2: " << endl;
	is >> ex.name2;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void RegRegCommand::execute() {
	if (!regBlock->isExist(name1))
		throw (exception("Register1 in R/R command does not exists"));
	if (!regBlock->isExist(name2))
		throw (exception("Register2 in R/R command does not exists"));
	op1 = &regBlock->registers.find(name1)->second;
	op2 = &regBlock->registers.find(name2)->second;
	op1->second = true;
	op2->second = true;
	fptr(&op1->first, &op2->first);
	regBlock->setFlags(op1->first, op2->first);
	op1->second = false;
	op2->second = false;
}

void RegRegCommand::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
}

ostream& operator<<(ostream&os, const RegMemCommand&ex){
	ex.print(os); 
	os << ex.name1 << endl;
	os << ex.name2 << endl;
	return os;
}

istream& operator>>(istream& is, RegMemCommand& ex) {
	ex.input(is);
	cout << "Enter register: " << endl;
	is >> ex.name1;
	if (!is.good())
		throw(exception("EOF occured"));
	cout << "Enter label: " << endl;
	is >> ex.name2;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void RegMemCommand::execute() {
	if (!regBlock->isExist(name1))
		throw (exception("Register in R/M command does not exists"));
	int a = dataBlock->getData(name2);
	dataBlock->changeLock(name2, true);
	op1 = &regBlock->registers.find(name1)->second;
	op1->second = true;
	fptr(&op1->first, &a);
	regBlock->setFlags(op1->first, a);
	op1->second = false;
	dataBlock->changeLock(name2, false);

}

void RegMemCommand::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
	dataBlock = const_cast<DataMem*>(&proc.dataMemory);
}

ostream& operator<<(ostream&os, const RegConstCommand&ex){
	ex.print(os);
	os << ex.name1 << endl;
	os << ex.op2 << endl;
	return os;
}

istream& operator>>(istream& is, RegConstCommand& ex) {
	ex.input(is);
	cout << "Enter register1: " << endl;
	is >> ex.name1;
	if (!is.good())
		throw(exception("EOF occured"));
	cout << "Enter const: " << endl;
	is >> ex.op2;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void RegConstCommand::execute() {
	if (!regBlock->isExist(name1))
		throw (exception("Register in R/C command does not exists"));
	op1 = &regBlock->registers.find(name1)->second;
	op1->second = true;
	fptr(&op1->first, &op2);
	regBlock->setFlags(op1->first, op2);
	op1->second = false;
}

void RegConstCommand::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
}

ostream& operator<<(ostream&os, const RegCommand&ex){
	ex.print(os);
	os << ex.name1 << endl;
	return os;
}

istream& operator>>(istream& is, RegCommand& ex) {
	ex.input(is);
	cout << "Enter register: " << endl;
	is >> ex.name1;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void RegCommand::execute() {
	if (!regBlock->isExist(name1))
		throw (exception("Register in R command does not exists"));
	op = &regBlock->registers.find(name1)->second;
	op->second = true;
	fptr(&op->first, nullptr);
	
	op->second = false;
}

void RegCommand::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
}

ostream& operator<<(ostream&os, const MemMemCommand&ex){
	ex.print(os);
	os << ex.name1 << endl; 
	os << ex.name2 << endl;
	return os;
}


istream& operator>>(istream& is, MemMemCommand& ex) {
	ex.input(is);
	cout << "Enter label1: " << endl;
	is >> ex.name1;
	if (!is.good())
		throw(exception("EOF occured"));
	cout << "Enter label2: " << endl;
	is >> ex.name2;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void MemMemCommand::execute() {
	int a, b;
	try{
		a = dataBlock->getData(name1);
		b = dataBlock->getData(name2);
	}
	catch (exception& bm){ throw bm; }
	dataBlock->changeLock(name1, true);
	dataBlock->changeLock(name2, true);
	fptr(&a, &b);
	regBlock->setFlags(a, b);
	tuple<char*, int, bool>* op1 = &dataBlock->data.find(name1)->second;
	char* trash = get<0>(*op1);
	int size = get<1>(*op1);
	*op1 = tuple<char*, int, bool>(nullptr, size, true);
	delete[] trash;
	char* ptr = new char[size];
	sprintf(ptr, "%d", a);
	
	*op1 = tuple<char*, int, bool>(ptr, size, false);
	dataBlock->changeLock(name2, false);
}

void MemMemCommand::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
	dataBlock = const_cast<DataMem*>(&proc.dataMemory);
}

ostream& operator<<(ostream&os, const MemRegCommand&ex){
	ex.print(os);
	os << ex.name1 << endl; 
	os << ex.name2 << endl;
	return os;
}

istream& operator>>(istream& is, MemRegCommand& ex) {
	ex.input(is);
	cout << "Enter label: " << endl;
	is >> ex.name1;
	if (!is.good())
		throw(exception("EOF occured"));
	cout << "Enter register: " << endl;
	is >> ex.name2;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void MemRegCommand::execute() {
	if (!regBlock->isExist(name2))
		throw (exception("Register in R/M command does not exists"));
	op2 = &regBlock->registers.find(name2)->second;
	int a = dataBlock->getData(name1);
	dataBlock->changeLock(name1, true);
	op2->second = true;
	fptr(&a, &op2->first);
	regBlock->setFlags(a, op2->first);
	tuple<char*, int, bool>* op1 = &dataBlock->data.find(name1)->second;
	char* trash = get<0>(*op1);
	int size = get<1>(*op1);
	*op1 = tuple<char*, int, bool>(nullptr, size, true);
	delete[] trash;
	char* ptr = new char[get<1>(*op1)+1];
	sprintf_s(ptr, get<1>(*op1) + 1, "%d", a);
	*op1 = tuple<char*, int, bool>(ptr, size, false);
	op2->second = false;
}

void MemRegCommand::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
	dataBlock = const_cast<DataMem*>(&proc.dataMemory);
}

ostream& operator<<(ostream&os, const MemConstCommand&ex){
	ex.print(os);
	os << ex.name1 << endl;
	os << ex.op2 << endl; 
	return os;
}

istream& operator>>(istream& is, MemConstCommand& ex) {
	ex.input(is);
	cout << "Enter label: " << endl;
	is >> ex.name1;
	if (!is.good())
		throw(exception("EOF occured"));
	cout << "Enter const: " << endl;
	is >> ex.op2;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}

void MemConstCommand::execute() {
	int a = dataBlock->getData(name1);
	dataBlock->changeLock(name1, true);
	fptr(&a, &op2);
	regBlock->setFlags(a, op2);
	tuple<char*, int, bool>* op1 = &dataBlock->data.find(name1)->second;
	char* trash = get<0>(*op1);
	int size = get<1>(*op1);
	
	*op1 = tuple<char*, int, bool>(nullptr, size, true);
	delete[] trash;
	char* ptr = new char[get<1>(*op1)+1];
	sprintf_s(ptr, get<1>(*op1) + 1, "%d", a);
	*op1 = tuple<char*, int, bool>(ptr, size, false);
}

void MemConstCommand::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	regBlock = const_cast<RegistersBlock*>(&proc.regBlock);
	dataBlock = const_cast<DataMem*>(&proc.dataMemory);
}

ostream& operator<<(ostream&os, const MemCommand&ex){
	ex.print(os);
	os << ex.name1 << endl;
	return os;
}

istream& operator>>(istream& is, MemCommand& ex) {
	ex.input(is);
	cout << "Enter label: " << endl;
	is >> ex.name1;
	if (!is.good())
		throw(exception("EOF occured"));
	return is;
}


void MemCommand::execute() {
	int a = dataBlock->getData(name1);
	dataBlock->changeLock(name1, true);
	fptr(&a, nullptr);
	tuple<char*, int, bool>* op1 = &dataBlock->data.find(name1)->second;
	char* trash = get<0>(*op1);
	int size = get<1>(*op1);
	*op1 = tuple<char*, int, bool>(nullptr, size, true);
	delete[] trash;
	char* ptr = new char[get<1>(*op1)+1];
	sprintf_s(ptr, get<1>(*op1) + 1, "%d", a);
	*op1 = tuple<char*, int, bool>(ptr, size, false);
}

void MemCommand::initCmd(const CPU& proc){
	auto val = instrTable.find(getCmdCode());
	if (val == instrTable.end())
		throw(exception("command not found"));
	fptr = val->second;
	dataBlock = const_cast<DataMem*>(&proc.dataMemory);
}