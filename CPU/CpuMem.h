#pragma once

#include "AbstractCommand.h"
#include "RegistersBlock.h"
//#include <vector>
#include "my_vector.h"
#include <map>
#include <sstream>

using namespace std;

//const vctr::vector<const string> __x86_TYPES = {
//	"REG_REG",
//	"REG_CONST",
//	"REG",
//	"MEM_MEM",
//	"MEM_CONST",
//	"MEM",
//	"MEM_REG",
//	"REG_MEM"
//};

enum cmdType
{
	REG_REG,
	REG_CONST,
	REG,
	MEM_MEM,
	MEM_CONST,
	MEM,
	MEM_REG,
	REG_MEM,
	GOTO_REG,
	GOTO_MEM, 
	GOTO_CONST
};

int __x86_INC(int *, int *);
int __x86_DEC(int *, int *);
int __x86_MOV(int *, int *);
int __x86_ADD(int *, int *);
int __x86_SUB(int *, int *);
int __x86_JG(int*, int *);
int __x86_JL(int*, int *);
int __x86_JE(int*, int *);
int __x86_JNE(int*, int *);
int __x86_JZ(int*, int *);
int __x86_CMP(int*, int*);
int __x86_INT(int*, int*);

const map<const string, int(*)(int*, int*) > instrTable = {
	{"INC", &__x86_INC },
	{ "DEC", &__x86_DEC },
	{"MOV", &__x86_MOV },
	{ "ADD", &__x86_ADD },
	{"SUB", &__x86_SUB },
	{"JG", &__x86_JG},
	{ "JL", &__x86_JL },
	{ "JE", &__x86_JE },
	{ "JNE", &__x86_JNE },
	{ "JZ", &__x86_JZ },
	{"JMP", nullptr},
	{"CMP", &__x86_CMP},
	{"INT", &__x86_INT}
};



class CpuMem
{
public:
	CpuMem(RegistersBlock* reg):IP(0), regs(reg){};
	CpuMem(CpuMem&);
	CpuMem():regs(nullptr){};
	~CpuMem(){ for (size_t i = 0; i < commands.size(); i++) delete commands[i]; };
	void addCmd(AbstractCommand*);
	void addCommands(const vctr::vector<AbstractCommand*>& newMem);
	void incIP() { IP++; }
	void setIP(int value){ IP = value; }
	int getIP(){ return IP; }
	int getInstrNum() { return commands.size(); }
	void executeGoToCmd(const string&);
	AbstractCommand* getCurrentCommand() const;
	void initLabels(map<string, pair<int, int> >* mapPtr);
	void flushMem();
private:
	vctr::vector<AbstractCommand*> commands;
	int IP = 0;
	RegistersBlock* regs;
	int getCmdFromLabel(const string&) const;
};


