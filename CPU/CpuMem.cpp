#include "CpuMem.h"
#include "my_vector.h"

void CpuMem::addCommands(const vctr::vector<AbstractCommand*>& newMem){
	flushMem();
	for (auto pos = newMem.cbegin(); pos != newMem.cend(); pos++) {
		AbstractCommand* ptr = (*pos)->clone();
		commands.push_back(ptr);
	}
}

void CpuMem::addCmd(AbstractCommand* newCmd){
	commands.push_back(newCmd);
}

int CpuMem::getCmdFromLabel(const string& target) const {
	for (int i = 0; i < commands.size(); i++) {
		if (commands[i]->getLabel() == target)
			return i;
	}
	throw(exception("target not found"));
}

void CpuMem::initLabels(map<string, pair<int, int> >* mapPtr) {
	for (auto pos = mapPtr->cbegin(); pos != mapPtr->cend(); pos++) {
		if (pos->second.second == 0) {
			if (pos->second.first >= commands.size())
				throw(exception("invalid adress for label"));
			commands[pos->second.first]->setLabel(pos->first);
		}
	}
}

void CpuMem::executeGoToCmd(const string& target) {
	int num = getCmdFromLabel(target);
	setIP(num);
}

void CpuMem::flushMem() {
	for (auto pos = commands.begin(); pos != commands.end(); pos++)
		delete (*pos);
	vctr::vector<AbstractCommand*> newValue;
	commands = newValue;
	IP = 0;
}

AbstractCommand* CpuMem::getCurrentCommand() const{
	try{
		commands[IP];
	}
	catch (exception&){ return nullptr; }
	return commands[IP];
}

ostringstream asmBuf;

int __x86_INC(int* first, int* second) {
	(*first)++;
	return 0;
}

int __x86_DEC(int* first, int* second) {
	(*first)--;
	return 0;
}


int __x86_MOV(int* first, int* second) {
	(*first) = (*second);
	return 0;
}

int __x86_ADD(int* first, int* second) {
	(*first) += (*second);
	return 0;
}

int __x86_SUB(int* first, int* second) {
	(*first) -= (*second);
	return 0;
}

int __x86_JG(int* first, int* second){
	if ((*first) == 1)
		return 1;
	return 0;
}

int __x86_JL(int* first, int* second){
	if ((*first) == -1)
		return 1;
	return 0;
}

int __x86_JE(int* first, int* second){
	if ((*first) == 0)
		return 1;
	return 0;
}

int __x86_JNE(int* first, int* second){
	if ((*first) != 0)
		return 1;
	return 0;
}


int __x86_JZ(int* first, int* second){
	if ((*second) == 0)
		return 1;
	return 0;
}

int __x86_CMP(int*, int*){ return 0; }

int __x86_INT(int* first, int* second) {
	switch (*first){
	case 1:
		if (second == nullptr)
			throw(exception("invalid operand 2"));
		asmBuf << *second;
		return 0;
	case 2:
		if (second == nullptr)
			throw(exception("invalid operand 2"));
		cin >> *second;
		return 0;
	case 3:
		cout << asmBuf.str();
	case 4:
		asmBuf.str();
	default:
		return 0;
	}
}