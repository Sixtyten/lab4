#include "RegistersBlock.h"

RegistersBlock::RegistersBlock()
{
	flags.insert({ "EF", { 0, false } });
	flags.insert({ "ZF", { 0, false } });
}

RegistersBlock::RegistersBlock(const string& fNameRegs, const string& fNameFlags) {
	ifstream inRegs(fNameRegs), inFlags(fNameFlags);
	if ((!inRegs.is_open()) || (!inFlags.is_open()))
		throw(exception("cannot open registers config file"));
	initRegisters(inRegs, inFlags);
	inRegs.close();
	inFlags.close();
}

int RegistersBlock::getValue(const string& name) const {
	auto pos = registers.find(name);
	if (pos == registers.end())
		throw(exception("register does not exist"));
	return pos->second.first;
}

int  RegistersBlock::getValueFlag(const string& name) const{
	auto pos = flags.find(name);
	if (pos == flags.end())
		throw(exception("register does not exist"));
	return pos->second.first;
}

void RegistersBlock::setFlags(int op1, int op2) {
	lockFlags(true);
	auto flagEf = &flags.find("EF")->second.first;
	if (op1 == op2)
		*flagEf = 0;
	if (op1 < op2)
		*flagEf = -1;
	if (op1 > op2)
		*flagEf = 1;
	lockFlags(false);
}

void RegistersBlock::print() {
	for (auto pos = registers.cbegin(); pos != registers.cend(); pos++)
		cout << pos->first << "\t" << pos->second.first << endl; 

	for (auto pos = flags.cbegin(); pos != flags.cend(); pos++)
		cout << pos->first << "\t" << pos->second.first << endl;
}

void RegistersBlock::initRegisters(istream& regs, istream& isFlags) {
	string line;
	while (getline(regs, line)) {
		registers.insert({ line, { 0, false } });
	}
	while (getline(isFlags, line)) {
		flags.insert({ line, { 0, false } });
	}
}

void RegistersBlock::zeroRegisters() {
	for (auto pos = registers.begin(); pos != registers.end(); pos++) {
		pos->second.first = 0;
		pos->second.second = false;
	}
	for (auto pos = flags.begin(); pos != flags.end(); pos++) {
		pos->second.first = 0;
		pos->second.second = false;
	}
}

bool  RegistersBlock::isLocked(const string& name)const{
	auto pos = registers.find(name);
	if (pos == registers.end())
		throw(exception("register not found"));
	return pos->second.second;
}

bool  RegistersBlock::isLockedFlag(const string& name)const{
	auto pos = flags.find(name);
	if (pos == flags.end())
		throw(exception("register not found"));
	return pos->second.second;
}

void RegistersBlock::addRegister(const string& name, const pair<int, bool>& value){
	if (registers.find(name) != registers.end())
		throw(exception("object is already exists"));
	registers.insert({name, value});
}