#pragma once

#include "AbstractCommand.h"

class ExecuteUnit
{
public:
	ExecuteUnit():isLocked(false){};
	~ExecuteUnit(){};
	AbstractCommand* getCommand(int num) const;
	void changeLock(bool value){ isLocked = value; }
	bool getLock(){ return isLocked; }
	void ExecuteCommand(AbstractCommand*);
private:
	bool isLocked;
};
