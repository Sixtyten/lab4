#include "CPU.h"
#include "Program.h"

void CPU::executeProgram(istream& is) {	
	passOne(is);
	passTwo();
}

vctr::vector<string> CPU::split(const string &s, char delim) {
	vctr::vector<string> elems;
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		if (item.length() > 0) {
			elems.push_back(item);
		}
	}
	return elems;
}

void CPU::cmdTableInit(const string& fileName) {
	string line;
	ifstream in(fileName);
	if (in.is_open()) {
		while (getline(in, line)) {
			vctr::vector<string> v = split(line, ' ');
			cmdTable.insert({ v[0], stoi(v[1]) });
		}
		in.close();
	}
}

void CPU::directiveTableInit(const string& fileName) {
	string line;
	ifstream in(fileName);
	if (in.is_open()) {
		while (getline(in, line)) {
			vctr::vector<string> v = split(line, ' ');
			directiveTable.insert({ v[0], stoi(v[1]) });
		}
		in.close();
	}
}

bool isComment(const string& line) {
	return line[0] == ';';
}

bool isLabel(const string& str) {
	return str.back() == ':' && str != ":";
}

bool CPU::checkType(const string& opCode, int type)const {
	for (auto pos = cmdTable.find(opCode); pos != cmdTable.end(); pos++){
		if (pos->first == opCode && pos->second == type)
			return true;
	}
	return false;
}

int CPU::extractType(const vctr::vector<string>& line) {
	if (line[0] == "END")
		return END_STATEMENT;
	auto pos = cmdTable.find(line[0]);
	switch (line.size()){
	case 1:
		if (isLabel(line[0]))
			return IS_LABEL;
	case 2:
		if (pos == cmdTable.end()) 
			throw (exception("cmd not found"));
		if (regBlock.isExist(line[1])) {
			if (checkType(line[0], REG))
				return REG;
			if (checkType(line[0], GOTO_REG))
				return GOTO_REG;
			throw(exception("type not found"));
		}
		try {
			stoi(line[1]);
		}
		catch (exception&) { 
			if(checkType(line[0], MEM))
				return MEM;
			if (checkType(line[0], GOTO_MEM))
				return GOTO_MEM;
			throw(exception("type not found"));
		}
		if (checkType(line[0], GOTO_CONST))
			return GOTO_CONST;
		throw(exception("invalid operand"));
	case 3:
		if (directiveTable.find(line[1]) != directiveTable.end() && line[0].back() != ':'  && line[0] != ":") {
			try {
				stoi(line[2]);
			}
			catch (exception&) { throw(exception("invalid value for directive")); }
			return IS_DIRECTIVE;
		}
		if (cmdTable.find(line[0]) == cmdTable.end())
			throw (exception("cmd not found"));
		if (regBlock.isExist(line[1])) {
			if (regBlock.isExist(line[2])) {
				if (checkType(line[0], REG_REG))
					return REG_REG;
				throw(exception("type not found"));
			}
			try {
				stoi(line[2]);
			}
			catch (exception&) {
				if (checkType(line[0], REG_MEM))
					return REG_MEM;
				throw(exception("type not found"));
			}
			if (checkType(line[0], REG_CONST))
				return REG_CONST;
			throw(exception("type not found"));
		}
		if (regBlock.isExist(line[2])) {
			try {
				stoi(line[1]);
			}
			catch (exception&) {
				if (checkType(line[0], MEM_REG))
					return MEM_REG;
				throw(exception("type not found"));
			}
			throw(exception("invalid operand 1"));
		}
		try {
			stoi(line[1]);
		}
		catch (exception& ) { 
			try {
				stoi(line[2]);
			}
			catch (exception&) {
				if (checkType(line[0], MEM_MEM))
					return MEM_MEM; 
				throw(exception("type not found"));
			}
			if (checkType(line[0], MEM_CONST))
				return MEM_CONST;
			throw(exception("type not found"));
		}
		throw(exception("invalid operand 1"));
	default:
		throw(exception("invalid line"));
	}
}

int CPU::getDirLength(const string& str) {
	auto pos = directiveTable.find(str);
	return pos->second;
}

void CPU::passOne(istream& os) {
	bool moreInput = true;
	string line, newLabel;
	int locationCnt = 0, type;
	cmdTableInit("cmd.txt");
	directiveTableInit("dir.txt");
	while (moreInput){
		getline(os, line);
		if ((!os.good()) && (line != "END"))
			throw(exception("EOF occured"));
		if (line == "")
			continue;
		
		if (!isComment(line)) {
			vctr::vector<string> splitted = split(line, ' ');	
			type = extractType(splitted);
			switch (type){
			case REG_REG:
				addRegRegCmd(splitted[0], splitted[1], splitted[2]); break;
			case REG_CONST:
				addRegConstCmd(splitted[0], splitted[1], stoi(splitted[2])); break;
			case REG:
				addRegCmd(splitted[0], splitted[1]); break;
			case MEM_MEM:
				addMemMemCmd(splitted[0], splitted[1], splitted[2]); break;
			case MEM_CONST:
				addMemConstCmd(splitted[0], splitted[1], stoi(splitted[2])); break;
			case MEM:
				addMemCmd(splitted[0], splitted[1]); break;
			case MEM_REG:
				addMemRegCmd(splitted[0], splitted[1], splitted[2]); break;
			case REG_MEM:
				addRegMemCmd(splitted[0], splitted[1], splitted[2]); break;
			case GOTO_REG:
				addGoToReg(splitted[0], splitted[1]); break;
			case GOTO_MEM:
				addGoToMem(splitted[0], splitted[1]); break;
			case GOTO_CONST:
				addGoToAdress(splitted[0], stoi(splitted[1])); break;
			case IS_DIRECTIVE:
				addVar(splitted[0], splitted[1], stoi(splitted[2])); continue;
			case IS_LABEL:
				newLabel = splitted[0].substr(0, splitted[0].size() - 1); 
				labelTable.insert({ newLabel, { locationCnt, 0 } }); continue;
			case END_STATEMENT:
				moreInput = false; continue;
			default:
				continue;
			}
			locationCnt++;
		}
	}
	cmdMemory.initLabels(&labelTable);
}

void CPU::passTwo() {
	while(cmdMemory.getIP() < cmdMemory.getInstrNum()) {
		AbstractCommand* ptr = cmdMemory.getCurrentCommand();
		cmdMemory.incIP();
		exUnit.ExecuteCommand(ptr);
	}
}

void CPU::addRegRegCmd(const string& opCode, const string& name1, const string& name2) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		RegRegCommand* ptr = new RegRegCommand(opCode, name1, name2, &regBlock, fptr);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addRegMemCmd(const string& opCode, const string& name1, const string& name2) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		RegMemCommand* ptr = new RegMemCommand(opCode, name1, name2, &regBlock, &dataMemory, fptr);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addRegCmd(const string& opCode, const string& name1) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		RegCommand* ptr = new RegCommand(opCode, name1, &regBlock, fptr);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addRegConstCmd(const string& opCode, const string& name1, int value) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		RegConstCommand* ptr = new RegConstCommand(opCode, name1, value, &regBlock, fptr);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addMemMemCmd(const string& opCode, const string& name1, const string& name2) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		MemMemCommand* ptr = new MemMemCommand(opCode, name1, name2, &dataMemory, fptr, &regBlock);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addMemRegCmd(const string& opCode, const string& name1, const string& name2) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		MemRegCommand* ptr = new MemRegCommand(opCode, name1, name2, &regBlock, &dataMemory, fptr);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addMemConstCmd(const string& opCode, const string& name1, int value) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		MemConstCommand* ptr = new MemConstCommand(opCode, name1, &dataMemory, value, fptr, &regBlock);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addMemCmd(const string& opCode, const string& name1) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		MemCommand* ptr = new MemCommand(opCode, name1, &dataMemory, fptr);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addGoToReg(const string& opCode, const string& target) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		GoToRegisterLabel* ptr = new GoToRegisterLabel(opCode, target, &regBlock, &cmdMemory, fptr);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addGoToMem(const string& opCode, const string& target) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		GoToLabel* ptr = new GoToLabel(opCode, target, &cmdMemory, fptr, &regBlock);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addGoToAdress(const string& opCode, int target) {
	auto pos = instrTable.find(opCode);
	if (pos != instrTable.end()) {
		int(*fptr)(int*, int*) = pos->second;
		GoToAdress* ptr = new GoToAdress(opCode, target, &cmdMemory, fptr, &regBlock);
		cmdMemory.addCmd(ptr);
		return;
	}
	throw(exception("cmd not found"));
}

void CPU::addVar(const string& name, const string& dirCode,  int value) {
	auto pos = directiveTable.find(dirCode);
	if (pos != directiveTable.end()) {
		labelTable.insert({ name, { value, pos->second } });
		dataMemory.allocateMemory(name, pos->second, value);
		return;
	}
	throw(exception("directive not found"));
}

void CPU::flushMemory() {
	dataMemory.flushData();
	regBlock.zeroRegisters();
	cmdMemory.flushMem();
	cmdMemory.setIP(0);
}

void CPU::loadAndRun(Program& prog) {
	cmdMemory.addCommands(prog.getVector());
	passTwo();
}

void CPU::loadData(int value, const string& opCode, const string& label) {
	if (directiveTable.empty())
		directiveTableInit("dir.txt");
	auto pos = directiveTable.find(opCode);
	if (pos == directiveTable.end())
		throw(exception("Directive not found"));
	int size = pos->second;
	dataMemory.allocateMemory(label, size, value);
}