#pragma once

#include "AbstractCommand.h"
#include "CpuMem.h"

class GoToRegisterLabel :
	public AbstractCommand
{
public:
	GoToRegisterLabel(const string& cmdName, const string& strName1, RegistersBlock* regs, CpuMem* cmdMem, int(*funcPtr)(int*, int*))
		:AbstractCommand(cmdName), target(strName1), regBlock(regs), cmdMemory(cmdMem), fptr(funcPtr) {};
	GoToRegisterLabel() :AbstractCommand(){};
	~GoToRegisterLabel(){};
	void execute();
	void initCmd(const CPU&);
	AbstractCommand* clone(){ GoToRegisterLabel* ptr = new GoToRegisterLabel(*this); return ptr; }
	friend ostream& operator<<(ostream&, const GoToRegisterLabel&);
	friend istream& operator>>(istream&, GoToRegisterLabel&);
private:
	string target;
	RegistersBlock* regBlock;
	CpuMem* cmdMemory;
	int(*fptr)(int*, int*);
};

class GoToLabel :
	public AbstractCommand
{
public:
	GoToLabel(const string& cmdName, const string& strName1, CpuMem* cmdMem, int(*funcPtr)(int*, int*), RegistersBlock* regs)
		:AbstractCommand(cmdName), target(strName1), cmdMemory(cmdMem), fptr(funcPtr), regBlock(regs){};
	GoToLabel() :AbstractCommand(){};
	~GoToLabel(){};
	void execute();
	void initCmd(const CPU&);
	AbstractCommand* clone(){ GoToLabel* ptr = new GoToLabel(*this); return ptr; }
	friend ostream& operator<<(ostream&, const GoToLabel&);
	friend istream& operator>>(istream&, GoToLabel&);
private:
	string target;
	RegistersBlock* regBlock;
	CpuMem* cmdMemory;
	int(*fptr)(int*, int*);
};

class GoToAdress :
	public AbstractCommand
{
public:
	GoToAdress(const string& cmdName, int tValue, CpuMem* memPtr, int(*funcPtr)(int*, int*), RegistersBlock* regs)
		:AbstractCommand(cmdName), target(tValue), cmdMemory(memPtr), fptr(funcPtr), regBlock(regs){};
	GoToAdress():AbstractCommand(){};
	~GoToAdress(){};
	void execute();
	void initCmd(const CPU&);
	AbstractCommand* clone(){ GoToAdress* ptr = new GoToAdress(*this); return ptr; }
	friend ostream& operator<<(ostream&, const GoToAdress&);
	friend istream& operator>>(istream&, GoToAdress&);
private:
	int target;
	RegistersBlock* regBlock;
	CpuMem* cmdMemory;
	int(*fptr)(int*, int*);
};