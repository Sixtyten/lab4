#pragma once

#include "AbstractCommand.h" 
#include <iterator>
using namespace std;

class CPU;

int getInt(int &a);
int dialog(const char *msgs[], int N);

class Program
{
public:
	Program(){};
	~Program(){};
	void addCmd();
	void runProgram(CPU& proc);
	const vctr::vector<AbstractCommand*>& getVector() const { return commands; }
private:
	vctr::vector<AbstractCommand*> commands;
	vctr::vector<pair<int, pair<string, string> > > data;
	void initCommands(const CPU&);
};