#pragma once

#include <iostream>
//#include <vector>
#include <string>
#include <map>
#include <fstream>
using namespace std;


class RegistersBlock
{
public:
	RegistersBlock();
	RegistersBlock(const string& fNameRegs, const string& fNameFlags);
	~RegistersBlock(){};
	void lock(int num);
	void addRegister(const string&, const pair<int, bool>&);
	void setRegister(int value, int num);
	int getValue(const string& name) const;
	int getValueFlag(const string& name) const;
	void setFlags(int op1, int op2);
	bool isExist(const string& str) { return registers.find(str) != registers.end() ? true : false; }
	void initRegisters(istream& , istream& );
	void zeroRegisters();
	bool isLocked(const string& name)const;
	bool isLockedFlag(const string& name)const;
	void print(); 
private:
	map<string, pair<int, bool> > registers;
	map<string, pair<int, bool> > flags;
	void lockFlags(bool value){ for (auto pos = flags.begin(); pos != flags.end(); pos++) pos->second.second = value; }
	friend class CpuMem;
	friend class RegCommand;
	friend class RegMemCommand;
	friend class RegRegCommand;
	friend class RegConstCommand;
	friend class MemRegCommand;
	friend class GoToRegisterLabel;
	friend class GoToAdress;
	friend class GoToLabel;
};