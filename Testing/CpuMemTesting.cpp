#include "stdafx.h"
#include "..\CPU\CPU.h"
#include "gtest\gtest.h"

TEST(CpuMemory, CpuMemMethods) {
	CpuMem testSample;
	auto fptr = instrTable.find("MOV")->second;
	RegistersBlock regBlock;
	RegCommand *cmd1 = new RegCommand("MOV", "AX", &regBlock, fptr);
	cmd1->setLabel("FirstLabel");
	ASSERT_EQ(0, testSample.getInstrNum());
	ASSERT_NO_THROW(testSample.addCmd(cmd1));
	ASSERT_EQ(1, testSample.getInstrNum());
	ASSERT_EQ(cmd1, testSample.getCurrentCommand());
	
	testSample.setIP(10);
	ASSERT_EQ(10, testSample.getIP());

	testSample.executeGoToCmd("FirstLabel");
	ASSERT_EQ(0, testSample.getIP());

	ASSERT_ANY_THROW(testSample.executeGoToCmd("ErrorLabel"));
	ASSERT_NO_THROW(testSample.flushMem());
	ASSERT_EQ(0, testSample.getIP());
	ASSERT_ANY_THROW(testSample.executeGoToCmd("FirstLabel"));
	ASSERT_EQ(nullptr, testSample.getCurrentCommand());
}

TEST(CpuMemory, TestVectorCmd) {
	auto fptr = instrTable.find("INC")->second;
	RegistersBlock regBlock;
	regBlock.addRegister("AX", { 0, false });
	RegCommand *cmd1 = new RegCommand("INC", "AX", &regBlock, fptr);

	AbstractCommand* cmd2 = cmd1->clone();
	cmd1->setLabel("FirstLabel");
	cmd2->setLabel("SecondLabel");

	DataMem data;
	data.allocateMemory("var1", 4, 10);
	auto fptr2 = instrTable.find("ADD")->second;
	MemConstCommand *cmd3 = new MemConstCommand("ADD", "var1", &data, 10, fptr2, &regBlock);
	cmd3->setLabel("ThirdLabel");

	vctr::vector<AbstractCommand*> cmdVector;
	cmdVector.push_back(cmd1);
	cmdVector.push_back(cmd2);
	cmdVector.push_back(cmd3);

	CpuMem testSample;
	ASSERT_NO_THROW(testSample.addCommands(cmdVector));
	ASSERT_EQ(3, testSample.getInstrNum());
	
	ASSERT_EQ(cmd1->getLabel() , testSample.getCurrentCommand()->getLabel());
	ASSERT_EQ(cmd1->getCmdCode(), testSample.getCurrentCommand()->getCmdCode());
	testSample.incIP();
	ASSERT_EQ(cmd2->getLabel(), testSample.getCurrentCommand()->getLabel());
	ASSERT_EQ(cmd2->getCmdCode(), testSample.getCurrentCommand()->getCmdCode());
	testSample.incIP();
	ASSERT_EQ(cmd3->getLabel(), testSample.getCurrentCommand()->getLabel());
	ASSERT_EQ(cmd3->getCmdCode(), testSample.getCurrentCommand()->getCmdCode());

	//ASSERT_NO_THROW(for_each(cmdVector.begin(), cmdVector.end(), [](AbstractCommand* ptr){delete ptr;}));
	delete cmdVector[0];
	delete cmdVector[1];
	delete cmdVector[2];

	ASSERT_EQ(3, testSample.getInstrNum());

	ASSERT_NO_THROW(testSample.executeGoToCmd("FirstLabel"));
	ASSERT_EQ(0, testSample.getIP());
	ASSERT_NO_THROW(testSample.executeGoToCmd("ThirdLabel"));
	ASSERT_EQ(2, testSample.getIP());

	ASSERT_ANY_THROW(testSample.executeGoToCmd("ErrorLabel"));

	ASSERT_NO_THROW(testSample.setIP(0));

	ExecuteUnit exUnit;
	ASSERT_NO_THROW(exUnit.ExecuteCommand(testSample.getCurrentCommand()));
	ASSERT_EQ(1, regBlock.getValue("AX"));
	testSample.incIP();
	ASSERT_NO_THROW(exUnit.ExecuteCommand(testSample.getCurrentCommand()));
	ASSERT_EQ(2, regBlock.getValue("AX"));
	testSample.incIP();
	ASSERT_NO_THROW(exUnit.ExecuteCommand(testSample.getCurrentCommand()));
	ASSERT_EQ(20, data.getData("var1"));

	ASSERT_NO_THROW(testSample.flushMem());

	ASSERT_EQ(0, testSample.getIP());
	ASSERT_ANY_THROW(testSample.executeGoToCmd("FirstLabel"));
	ASSERT_ANY_THROW(testSample.executeGoToCmd("SecondLabel"));
	ASSERT_ANY_THROW(testSample.executeGoToCmd("ThirdLabel"));
	ASSERT_EQ(nullptr, testSample.getCurrentCommand());
}