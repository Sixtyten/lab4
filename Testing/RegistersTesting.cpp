#include "stdafx.h"
#include "..\CPU\CPU.h"
#include "gtest\gtest.h"

vctr::vector<string> initRegVector(const string& fName){
	ifstream is(fName);
	string buf;
	vctr::vector<string> res;
	if (is.is_open()) {
		getline(is, buf);
		res.push_back(buf);
	}
	return res;
}

TEST(RegistersMethods, Constructor) 
{
	RegistersBlock regBlock("regs.txt", "flags.txt");
	regBlock.zeroRegisters();
	auto names = initRegVector("regs.txt");
	auto namesFlags = initRegVector("flags.txt");
	for (auto pos = names.begin(); pos != names.end(); pos++) {
		ASSERT_NO_THROW(regBlock.getValue(*pos));
		ASSERT_EQ(0, regBlock.getValue(*pos));
	}
	for (auto pos = namesFlags.begin(); pos != namesFlags.end(); pos++) {
		ASSERT_NO_THROW(regBlock.getValueFlag(*pos));
		ASSERT_EQ(0, regBlock.getValueFlag(*pos));
	}
}

TEST(RegistersMethods, Blocking) {
	RegistersBlock regBlock("regs.txt", "flags.txt");
	auto names = initRegVector("regs.txt");
	auto namesFlags = initRegVector("flags.txt");
	for (auto pos = names.begin(); pos != names.end(); pos++) {
		ASSERT_NO_THROW(regBlock.isLocked(*pos));
		ASSERT_EQ(false, regBlock.isLocked(*pos));
	}
	for (auto pos = namesFlags.begin(); pos != namesFlags.end(); pos++) {
		ASSERT_NO_THROW(regBlock.isLockedFlag(*pos));
		ASSERT_EQ(false, regBlock.isLockedFlag(*pos));
	}
}