#include "stdafx.h"
#include "gtest\gtest.h"
#include "..\CPU\my_vector.h"

using namespace std;

TEST(VectorTesting, TestConstIteratorExc){
	vctr::vector<int> v1;
	auto pos = v1.cbegin();
	ASSERT_ANY_THROW(*pos);
	ASSERT_ANY_THROW(pos.operator->());
	ASSERT_ANY_THROW(pos[0]);
	ASSERT_ANY_THROW(pos++);
	ASSERT_ANY_THROW(++pos);
	ASSERT_ANY_THROW(pos + 1);
	ASSERT_ANY_THROW(pos - 1);
}

TEST(VectorTesting, TestIteratorExc){
	vctr::vector<int> v1;
	auto pos = v1.begin();
	ASSERT_ANY_THROW(*pos);
	ASSERT_ANY_THROW(pos.operator->());
	ASSERT_ANY_THROW(pos[0]);
	ASSERT_ANY_THROW(pos++);
	ASSERT_ANY_THROW(++pos);
	ASSERT_ANY_THROW(pos + 1);
	ASSERT_ANY_THROW(pos - 1);
}

TEST(VectorTesting, TestConstIterator){
	vctr::vector<int> testSample;
	for (int i = 0; i < 10; i++) {
		testSample.push_back(i);
	}
	int i = 0;
	for (auto pos = testSample.cbegin(); pos != testSample.cend(); pos++, i++) {
		ASSERT_EQ(i, *pos);
	}
	i = 0;
	for (auto pos = testSample.cbegin(); i < 10;  i++) {
		ASSERT_EQ(i, pos[i]);
		ASSERT_EQ(i, *(pos + i));
	}
	i = 10;
	int j = 0;
	for (auto pos = testSample.cend(); i > 0; i--, j++) {
		ASSERT_EQ(j, *(pos - i));
	}

	auto pos1 = testSample.cbegin();
	auto pos2 = testSample.cbegin();
	for (; pos1 != testSample.cend() && pos2 != testSample.cend(); pos1++, pos2++)
		ASSERT_EQ(true, pos2 == pos1);
}

TEST(VectorTesting, TestIterator){
	vctr::vector<int> testSample;
	for (int i = 0; i < 10; i++)
		testSample.push_back(i);
	int i = 0;
	for (auto pos = testSample.begin(); pos != testSample.end(); pos++, i++) {
		ASSERT_EQ(i, *pos);
	}
	i = 0;
	for (auto pos = testSample.begin(); i < 10; i++) {
		ASSERT_EQ(i, pos[i]);
		ASSERT_EQ(i, *(pos + i));
	}
	i = 10;
	int j = 0;
	for (auto pos = testSample.end(); i > 0; i--, j++) {
		ASSERT_EQ(j, *(pos - i));
	}

	auto pos1 = testSample.begin();
	auto pos2 = testSample.begin();
	for (; pos1 != testSample.end() && pos2 != testSample.end(); pos1++, pos2++)
		ASSERT_EQ(true, pos2 == pos1);
}

TEST(VectorTesting, Constructors) {
	vctr::vector<int> testSample1;
	ASSERT_EQ(true, testSample1.empty());
	ASSERT_EQ(0, testSample1.size());
	ASSERT_ANY_THROW(testSample1[0]);

	vctr::vector<int> testSample2;
	for (int i = 0; i < 10; i++)
		testSample2.push_back(i);

	vctr::vector<int> testSample3(testSample2);
	ASSERT_EQ(testSample3.size(), testSample2.size());
	for (int i = 0; i < 10; i++)
		ASSERT_EQ(testSample2[i], testSample3[i]);
}

TEST(VectorTesting, RValueAssign){
	vctr::vector<int> testSample;
	{
		vctr::vector<int> willBeDestroyed;
		for (int i = 0; i < 10; i++)
			willBeDestroyed.push_back(i);

		testSample = willBeDestroyed;
	}
	for (int i = 0; i < 10; i++)
		ASSERT_EQ(testSample[i], i);
}

TEST(VectorTesting, Assign){
	vctr::vector<int> obj;
	for (int i = 0; i < 10; i++)
		obj.push_back(i);
	vctr::vector<int> testSample;
	testSample = obj;
	for (int i = 0; i < 10; i++) {
		ASSERT_EQ(testSample[i], i);
		ASSERT_EQ(testSample[i], obj[i]);
	}
}

TEST(VectorTesting, VectorMethods){
	vctr::vector<int> testSample;
	for (int i = 0; i < 10; i++)
		testSample.push_back(i);
	
	testSample.clear();
	ASSERT_EQ(true, testSample.empty());
	ASSERT_EQ(0, testSample.size());

	for (int i = 0; i < 10; i++)
		testSample.push_back(i);

	auto pos = testSample.begin() + 5;
	auto pos2 = testSample.erase(pos);
	
	for (int i = 6; pos2 != testSample.end(); pos2++, i++)
		ASSERT_EQ(i, *pos2);
}